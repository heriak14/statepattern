package com.epam.view;

import com.epam.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    private Task task;

    public MyView(){
        task = new Task();
        menu = Menu.getMenu();
        setMenuMethods();
    }

    private void setMenuMethods(){
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::pushToDo);
        menuMethods.put("2", this::pushInProgress);
        menuMethods.put("3", this::reviewTask);
        menuMethods.put("4", this::testTask);
        menuMethods.put("5", this::doneTask);
        menuMethods.put("6", this::blockTask);
        menuMethods.put("Q", this::quit);
    }

    private void printMenu() {
        logger.info("_______________________"
                + "TASKS MENU______________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            logger.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.info("____________________________________________\n");
    }

    private void pushToDo(){
        task.pushToDo();
    }

    private void pushInProgress(){
        task.pushInProgress();
    }

    private void reviewTask(){
        task.reviewTask();
    }

    private void testTask(){
        task.testTask();
    }

    private void doneTask(){
        task.doneTask();
    }

    private void blockTask(){
        task.blockTask();
    }

    private void quit(){
        logger.info("Bye!");
    }

    public void show() {
        String option = "";
        do {
            printMenu();
            logger.info("Choose one option: ");
            option = SCANNER.next().toUpperCase();
            if(menuMethods.containsKey(option)){
                menuMethods.get(option).print();
            }else {
                logger.info("Wrong input! Try again.");
            }
        } while (!option.equals("Q"));
    }
}
