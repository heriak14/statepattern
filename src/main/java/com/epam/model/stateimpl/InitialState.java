package com.epam.model.stateimpl;

import com.epam.model.State;
import com.epam.model.Task;

import java.util.Scanner;

public class InitialState implements State {
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void pushToDo(Task task) {
        logger.info("Enter the task to do: ");
        String toDo = SCANNER.nextLine();
        logger.info("Enter the name of the person, that will do this task: ");
        String workerName = SCANNER.next();
        task.setValues(toDo, workerName);
        task.setState(new ToDoState());
        logger.info("New task for "+task.getWorkerName()+" was added!\n");
    }
}
