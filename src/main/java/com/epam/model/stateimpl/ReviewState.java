package com.epam.model.stateimpl;

import com.epam.model.State;
import com.epam.model.Task;

public class ReviewState implements State {

    @Override
    public void pushInProgress(Task task){
        task.setState(new ProgressState());
        logger.info(task.getWorkerName() + " started Redoing '"
                + task.getValue() + "' task again(pushed back into Progress State).\n");
    }

    @Override
    public void testTask(Task task){
        task.setState(new TestState());
        logger.info("...Testing '" + task.getValue() + "' by "
                + task.getWorkerName() + ".\n");
    }

    @Override
    public void blockTask(Task task){
        task.setState(new BlockState());
        logger.info("'"+task.getValue() + "' task was blocked by " + task.getWorkerName() + "!\n");
    }
}
