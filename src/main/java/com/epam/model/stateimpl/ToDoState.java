package com.epam.model.stateimpl;

import com.epam.model.State;
import com.epam.model.Task;

public class ToDoState implements State {

    @Override
    public void pushInProgress(Task task){
        task.setState(new ProgressState());
        logger.info(task.getWorkerName() + " started doing '"
                + task.getValue() + "' task(pushed in Progress State).\n");
    }

    @Override
    public void blockTask(Task task){
        task.setState(new BlockState());
        logger.info("'"+task.getValue() + "' task was blocked by " + task.getWorkerName() + "!\n");
    }
}
