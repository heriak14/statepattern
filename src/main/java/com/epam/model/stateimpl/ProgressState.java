package com.epam.model.stateimpl;

import com.epam.model.State;
import com.epam.model.Task;

public class ProgressState implements State {

    @Override
    public void reviewTask(Task task){
        task.setState(new ReviewState());
        logger.info("...Reviewing '" + task.getValue() + "' by "
                + task.getWorkerName() + ".\n");
    }

    @Override
    public void blockTask(Task task){
        task.setState(new BlockState());
        logger.info("'"+task.getValue() + "' task was blocked by " + task.getWorkerName() + "!\n");
    }
}
