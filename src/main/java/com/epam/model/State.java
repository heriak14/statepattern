package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {
    Logger logger = LogManager.getLogger(State.class);

    default void pushToDo(Task task){
        logger.info("pushToDo - is not allowed!\n");
    }

    default void pushInProgress(Task task){
        logger.info("pushInProgress - is not allowed!\n");
    }

    default void reviewTask(Task task){
        logger.info("reviewTask - is not allowed!\n");
    }

    default void testTask(Task task){
        logger.info("testTask - is not allowed!\n");
    }

    default void doneTask(Task task){
        logger.info("doneTask - is not allowed!\n");
    }

    default void blockTask(Task task){
        logger.info("blockTask - is not allowed!\n");
    }
}
