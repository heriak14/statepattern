package com.epam.model;

import com.epam.model.stateimpl.InitialState;

public class Task {
    private State state;
    private String value;
    private String workerName;

    public Task() {
        state = new InitialState();
    }

    public void setState(State newState) {
        state = newState;
    }

    public void setValues(String value, String workerName) {
        this.value = value;
        this.workerName = workerName;
    }

    public void pushToDo() {
        state.pushToDo(this);
    }

    public void pushInProgress() {
        state.pushInProgress(this);
    }

    public void reviewTask() {
        state.reviewTask(this);
    }

    public void testTask() {
        state.testTask(this);
    }

    public void doneTask() {
        state.doneTask(this);
    }

    public void blockTask() {
        state.blockTask(this);
    }

    @Override
    public String toString() {
        return "Task: " + value + "; Worker name: " + workerName;
    }

    public String getWorkerName() {
        return workerName;
    }

    public String getValue() {
        return value;
    }
}
